from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, HTTPException
import os
import json

app = FastAPI()

origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/api/posts")
def posts_index(page: int = 1, page_size: int = 5):
    with open(f'{os.getcwd()}/api/data.json', "r") as file:
        data = json.load(file)

    start = (page - 1) * page_size
    end = start + page_size
    posts = data[start:end]

    return {
        "posts": posts,
        "more_available": end < len(data)
    }

@app.put("/api/posts/{post_url}")
def hug_post(post_url: str):
    matching_post_index = None
    with open(f'{os.getcwd()}/api/data.json', "r") as file:
        data = json.load(file)
        for i, post in enumerate(data):
            if post['post_url'] == post_url:
                matching_post_index = i
                break
    if matching_post_index == None:
        raise HTTPException(status_code=404, detail="Post not found")
    else:
        with open(f'{os.getcwd()}/api/data.json', "w") as file:
            data[matching_post_index]['num_hugs'] += 1
            json.dump(data, file)

    return { "post_that_was_hugged": data[matching_post_index] }