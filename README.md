# Matts Takehome

This is my submission for the AuxHealth/MD&ME [takehome project](takehome.pdf).

The backend uses fastapi/nextjs for server rendering and was created with the following boilerplate: 
https://vercel.com/templates/next.js/nextjs-fastapi-starter

The frontend uses React and [tailwind](https://tailwindcss.com)/[daisyui](https://daisyui.com) to create a timeline of medical symptoms posts:
![screenshot](screenshot.png)

## Running the project
First, install the dependencies:

```
npm install
```

Then, run the development server:
```
npm run dev
```

Open http://localhost:3000 with your browser to see the result.