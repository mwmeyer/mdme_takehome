"use client"

import { useState, useEffect } from 'react';

export async function getPosts(page = 1) {
  const res = await fetch(`http://localhost:8000/api/posts?page=${page}`, {
    cache: 'no-store',
  });
  const postsData = await res.json();
  return { postsList: postsData?.posts, moreAvailable: postsData?.more_available };
}

export default function Home() { 
  const [postsList, setPostsList] = useState([]);
  const [page, setPage] = useState(1);
  const [hasMore, setHasMore] = useState(true);

  const fetchMorePosts = async () => {
    const { postsList, moreAvailable } = await getPosts(page);
    setPostsList((prevPosts) => [...prevPosts, ...postsList]);
    setPage((prevPage) => prevPage + 1);
    setHasMore(moreAvailable);
  };

  useEffect(() => {
    fetchMorePosts();
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      const scrollHeight = document.documentElement.scrollHeight;
      const scrollTop = document.documentElement.scrollTop;
      const clientHeight = document.documentElement.clientHeight;
  
      if (scrollTop + clientHeight >= scrollHeight && hasMore) {
        fetchMorePosts();
      }
    };
  
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [page, hasMore]);

  const handleHug = async (url) => {
    const res = await fetch(`http://localhost:8000/api/posts/${url}`, {
      method: 'PUT',
    });
    const updateResponse = await res.json()
    setPostsList((prevPosts) =>
      prevPosts.map((post) =>
        post.post_url === url ? updateResponse.post_that_was_hugged : post
      )
    );
  };  

  return (
    <main className="flex min-h-screen flex-col items-center justify-between">

      <div className="navbar bg-base-100">
        <div className="navbar-start">
          <a className="btn btn-ghost text-xl">./md_takehome</a>
        </div>
        <div className="navbar-end">
          <a href="/docs" target="_blank" className="btn">View API Docs</a>
        </div>
      </div>

      <section className="p-24">
      <div className="bg-slate-500 rounded-xl p-8">
      <ul className="timeline timeline-snap-icon max-md:timeline-compact timeline-vertical">
          {postsList.map((post, index) => (
            <li key={post.post_url}>
              {index !== 0 && <hr />}
              <div className="timeline-middle">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                  className="h-5 w-5"
                >
                  <path
                    fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
              <div className={`${index % 2 === 0 ? 'timeline-start md:text-end' : 'timeline-end'}`}>
                <time className="font-mono italic">{post.year}</time>
                <div className="text-lg font-black">{post.title}</div>
                <p>{post.patient_description}</p>
                <div className="card-actions justify-end">
                  <button
                    className="btn btn-primary"
                    onClick={() => handleHug(post.post_url)}
                  >
                    <span>{post.num_hugs}</span> Hug
                  </button>
                </div>
              </div>
              <hr/>
            </li>
          ))}
          {!hasMore && <li className="flex flex-row justify-center items-center"><hr/><div className="timeline-end timeline-box">The End</div></li>}
        </ul>
      </div>
      </section>

    </main>
  );
}
